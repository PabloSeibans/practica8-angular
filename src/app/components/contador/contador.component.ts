import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  numero: number = 0;
  mensaje: string = '';
  sumar(x: number){
    this.mensaje = '';
    if (x<50) {
      this.numero++;
    } else {
      this.mensaje = 'No se puede sobrepasar el limite';
    }
  }

  restar(x: number){
    this.mensaje = '';
    if (x>0) {
      this.numero--;
    } else {
      this.mensaje = 'El valor no puede ser menor a 0'; 
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
